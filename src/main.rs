extern crate piston_window;
#[cfg(feature="piston")]
#[macro_use] extern crate conrod;
extern crate simple_file_navigator;

use std::path::Path;

use piston_window::{EventLoop, PistonWindow, UpdateEvent, WindowSettings};

fn main() {
    const WIDTH: u32 = 600;
    const HEIGHT: u32 = 300;

    // Construct the window.
    let mut window: PistonWindow =
        WindowSettings::new("moo", [WIDTH, HEIGHT])
            .opengl(piston_window::OpenGL::V3_2)
            .vsync(true)
            .samples(4)
            .exit_on_esc(true)
            .build()
            .unwrap();
    window.set_ups(60);

    // Construct our `Ui`.
    let mut ui = conrod::UiBuilder::new().build();

    // A unique identifier for each widget.
    widget_ids!(struct Ids { canvas, file_navigator });
    let ids = Ids::new(ui.widget_id_generator());

    // Add a `Font` to the `Ui`'s `font::Map` from file.
    let assets = Path::new("assets");
    let font_path = assets.join("fonts/C64_Pro-STYLE.ttf");
    ui.fonts.insert_from_file(font_path).unwrap();

    // Create a texture to use for efficiently caching text on the GPU.
    let mut text_texture_cache =
        conrod::text::GlyphCache::new(&mut window, WIDTH, HEIGHT);

    // The image map describing each of our widget->image mappings (in our case, none).
    let image_map = conrod::image::Map::new();

    let directory = Path::new("/home/enrico/Downloads");

    // Poll events from the window.
    while let Some(event) = window.next() {

        // Convert the piston event to a conrod event.
        if let Some(e) = conrod::backend::piston::event::convert(event.clone(), &window) {
            ui.handle_event(e);
        }

        event.update(|_| {
            use conrod::{widget, Colorable, Positionable, Sizeable, Widget};
            use simple_file_navigator::SimpleFileNavigator;

            // Instantiate the conrod widgets.
            let ui = &mut ui.set_widgets();

            widget::Canvas::new().color(conrod::color::DARK_CHARCOAL).set(ids.canvas, ui);

            // Navigate the conrod directory only showing `.rs` and `.toml` files.
            for event in SimpleFileNavigator::with_extension(&directory, &["zip"])
                .color(conrod::color::LIGHT_BLUE)
                .font_size(16)
                .wh_of(ids.canvas)
                .middle_of(ids.canvas)
                //.show_hidden_files(true)  // Use this to show hidden files
                .set(ids.file_navigator, ui)
            {
                println!("{:?}", event);
            }
        });

        window.draw_2d(&event, |c, g| {
            if let Some(primitives) = ui.draw_if_changed() {
                fn texture_from_image<T>(img: &T) -> &T { img };
                conrod::backend::piston_window::draw(c, g, primitives,
                                                     &mut text_texture_cache,
                                                     &image_map,
                                                     texture_from_image);
            }
        });
    }

}
